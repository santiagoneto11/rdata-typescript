import {
  EMR,
  Instance,
  LastestOrders,
  LatestServices,
  TasksProgress,
  TotalProfit,
} from "../../shared/components";

import { Box, Container, Grid } from "@mui/material";
import { LayoutPageBase } from "../../shared/layouts";

export const Dashboard = () => {
  return (
    <LayoutPageBase>
      <>
        <Box
          component="main"
          sx={{
            flexGrow: 1,
            py: 8,
          }}
        >
          <Container maxWidth={false}>
            <Grid container spacing={3}>
              <Grid item lg={3} sm={6} xl={3} xs={12}>
                <Instance />
              </Grid>
              <Grid item xl={3} lg={3} sm={6} xs={12}>
                <EMR />
              </Grid>
              <Grid item xl={3} lg={3} sm={6} xs={12}>
                <TasksProgress />
              </Grid>
              <Grid item xl={3} lg={3} sm={6} xs={12}>
                <TotalProfit />
              </Grid>
              {/* <Grid item lg={8} md={12} xl={9} xs={12}>
                <Sales />
              </Grid>
              <Grid item lg={4} md={6} xl={3} xs={12}>
                <TrafficByDevice />
              </Grid> */}
              <Grid item lg={4} md={6} xl={3} xs={12}>
                <LatestServices />
              </Grid>
              <Grid item lg={8} md={12} xl={9} xs={12}>
                <LastestOrders />
              </Grid>
            </Grid>
          </Container>
        </Box>
      </>
    </LayoutPageBase>
  );
};
