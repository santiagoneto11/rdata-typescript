export * from "./dashboard/Dashboard";
export * from "./services/Services";
export * from "./account/Account";
export * from "./settings/Settings";
export * from "./login/Login";
export * from "./register/Register";
export * from "./404/404";