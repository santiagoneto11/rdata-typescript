import { Box, Container, Grid, Pagination } from "@mui/material";
import { LayoutPageBase } from "../../shared/layouts";
import { ServiceListToolbar, ServiceCard } from "../../shared/components";
import { services } from "../../__mocks__/services";

interface IServiceCardProps {
  id: string;
  createdAt: string;
  description: string;
  media: string;
  title: string;
  totalClicks: string;
}

export const Services: React.FC = () => {
  return (
    <LayoutPageBase>
      <Box
        component="main"
        sx={{
          flexGrow: 1,
          py: 8,
        }}
      >
        <Container maxWidth={false}>
          <ServiceListToolbar />
          <Box sx={{ pt: 3 }}>
            <Grid container spacing={3}>
              {services.map((service: IServiceCardProps) => (
                <Grid item key={service.id} lg={4} md={6} xs={12}>
                  <ServiceCard
                    title={service.title}
                    media={service.media}
                    description={service.description}
                    totalClicks={service.totalClicks}
                  />
                </Grid>
              ))}
            </Grid>
          </Box>

          <Box
            sx={{
              display: "flex",
              justifyContent: "center",
              pt: 3,
            }}
          >
            <Pagination color="primary" count={3} size="small" />
          </Box>
        </Container>
      </Box>
    </LayoutPageBase>
  );
};
