import { Box, Container, Typography } from "@mui/material";
import { SettingsNotifications, SettingsPassword } from "../../shared/components";
import { LayoutPageBase } from "../../shared/layouts";

export const Settings: React.FC = () => {
  return (
    <LayoutPageBase>
      <>
        <Box
          component="main"
          sx={{
            flexGrow: 1,
            py: 8,
          }}
        >
          <Container maxWidth="lg">
            <Typography sx={{ mb: 3 }} variant="h4">
              Configurações
            </Typography>
            <SettingsNotifications />
            <Box sx={{ pt: 3 }}>
              <SettingsPassword />
            </Box>
          </Container>
        </Box>
      </>
    </LayoutPageBase>
  );
};
