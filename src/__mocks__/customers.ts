import { v4 as uuid } from "uuid";

export const customers = [
  {
    id: uuid(),
    address: {
      country: "Brasil",
      state: "Ceará",
      city: "Fortaleza",
      street: "Rua X, 100",
    },
    avatarUrl: "/static/images/avatars/santiago.png",
    createdAt: 1555016400000,
    email: "santiago@empresa.com",
    name: "Santiago Neto",
    phone: "85 9 81482543",
  },
  {
    id: uuid(),
    address: {
      country: "Brasil",
      state: "",
      city: "",
      street: "",
    },
    avatarUrl: "/static/images/avatars/tagliani.png",
    createdAt: 1555016400000,
    email: "tagliani@empresa.com",
    name: "Gabriel Tagliani",
    phone: "(xx) 9xxxx-xxxx)",
  },
  {
    id: uuid(),
    address: {
      country: "Brasil",
      state: "",
      city: "",
      street: "",
    },
    avatarUrl: "/static/images/avatars/vini.png",
    createdAt: 1555016400000,
    email: "vinicios@empresa.io",
    name: "Vinicios (io) Campos",
    phone: "(xx) 9xxxx-xxxx)",
  },
];
