import { v4 as uuid } from "uuid";

interface IServiceCardProps {
  id: string;
  createdAt: string;
  description: string;
  media: string;
  title: string;
  totalClicks: string;
}

export const services: IServiceCardProps[] = [
  {
    id: uuid(),
    createdAt: "27/03/2019",
    description:
      "Vini.io é um serviço de criação de pipelines de forma automática por meio da criação de JSONs.",
    media: "/static/images/services/config.png",
    title: "Vini.io",
    totalClicks: "594",
  },
  {
    id: uuid(),
    createdAt: "31/03/2019",
    description:
      "O Manta é uma plataforma de dados criado pelo time de dados da RD onde serão possíveis a utilização de diversas funcionalidades.",
    media: "/static/images/services/config.png",
    title: "Manta",
    totalClicks: "625",
  },
  {
    id: uuid(),
    createdAt: "03/04/2019",
    description: "O Teams é a plataforma de comunicação principal usada na RD.",
    media: "/static/images/services/teams.png",
    title: "Teams",
    totalClicks: "857",
  },
  {
    id: uuid(),
    createdAt: "04/04/2019",
    description:
      "A AWS é o serviço de cloud-computing usado para armazenar e executar todos os processos e dados do Data Lake e Delta Lake da RD.",
    media: "/static/images/services/aws.png",
    title: "AWS",
    totalClicks: "406",
  },
  {
    id: uuid(),
    createdAt: "04/04/2019",
    description:
      "O Gitlab é o repositório de códigos e controle de versão usado na RD.",
    media: "/static/images/services/gitlab.png",
    title: "GitLab",
    totalClicks: "835",
  },
  {
    id: uuid(),
    createdAt: "04/04/2019",
    description:
      "O Office 365 é onde estão centralizados os serviços microsoft utilizados pela RD. Exemplos: Teams, Outlook e Sharepoint.",
    media: "/static/images/services/office365.png",
    title: "Office 365",
    totalClicks: "835",
  },
];
