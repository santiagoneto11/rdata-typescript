import { v4 as uuid } from "uuid";

export const orders = [
  {
    id: uuid(),
    ref: "LMB1049",
    amount: 30.5,
    customer: {
      name: "Lambda Data Lake",
    },
    createdAt: 1648216623000,
    status: "rodando",
  },
  {
    id: uuid(),
    ref: "EMR1048",
    amount: 25.1,
    customer: {
      name: "EMR QA",
    },
    createdAt: 1643122301000,
    status: "finalizado",
  },
  {
    id: uuid(),
    ref: "LMB1047",
    amount: 10.99,
    customer: {
      name: "Lambda Query Athena",
    },
    createdAt: 1643122301000,
    status: "falha",
  },
  {
    id: uuid(),
    ref: "STF1046",
    amount: 96.43,
    customer: {
      name: "Step Functions Delta",
    },
    createdAt: 1648216623000,
    status: "pendente",
  },
  {
    id: uuid(),
    ref: "CLW1045",
    amount: 32.54,
    customer: {
      name: "CloudWatch logs",
    },
    createdAt: 1648216623000,
    status: "finalizado",
  },
  {
    id: uuid(),
    ref: "DDB1044",
    amount: 16.76,
    customer: {
      name: "DynamoDB Logs",
    },
    createdAt: 1648216623000,
    status: "finalizado",
  },
];
