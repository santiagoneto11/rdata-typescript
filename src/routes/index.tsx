import { Routes, Route, Navigate } from "react-router-dom";
import { useDrawerContext } from "../shared/contexts";
import { useEffect } from "react";

import {
  Account,
  Dashboard,
  Login,
  NotFound,
  Register,
  Services,
  Settings,
} from "../pages";

import {
  ChartBar as ChartBarIcon,
  Cog as CogIcon,
  Lock as LockIcon,
  ShoppingBag as ShoppingBagIcon,
  User as UserIcon,
  UserAdd as UserAddIcon,
  XCircle as XCircleIcon,
} from "../shared/icons";

export const AppRoutes = () => {
  const { setDrawerOptions } = useDrawerContext();

  useEffect(() => {
    setDrawerOptions([
      {
        label: "Serviços",
        icon: <ShoppingBagIcon fontSize="small" />,
        path: "/services",
      },
      {
        label: "Relatórios",
        icon: <ChartBarIcon fontSize="small" />,
        path: "/dashboard",
      },
      {
        label: "Conta",
        icon: <UserIcon fontSize="small" />,
        path: "/account",
      },
      {
        label: "Configurações",
        icon: <CogIcon fontSize="small" />,
        path: "/settings",
      },
      {
        label: "Login",
        icon: <LockIcon fontSize="small" />,
        path: "/login",
      },
      {
        label: "Registro",
        icon: <UserAddIcon fontSize="small" />,
        path: "/register",
      },
      {
        label: "Erro",
        icon: <XCircleIcon fontSize="small" />,
        path: "/404",
      },
    ]);
  }, []);

  return (
    <Routes>
      <Route path="/services" element={<Services />} />
      <Route path="/dashboard" element={<Dashboard />} />
      <Route path="/account" element={<Account />} />
      <Route path="/settings" element={<Settings />} />
      <Route path="/login" element={<Login />} />
      <Route path="/register" element={<Register />} />
      <Route path="/404" element={<NotFound />} />
      <Route path="/" element={<Navigate to="dashboard" />} />
      <Route path="*" element={<Navigate to="404" />} />
    </Routes>
  );
};
