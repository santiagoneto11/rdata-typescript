import { Box } from "@mui/system";
import { NavBar, SideBar } from "../components";

export const LayoutPageBase: React.FC = ({ children }) => {
  return (
    <Box height="100%" display="flex" flexDirection="column">
      <SideBar>
        <NavBar />
        {children}
      </SideBar>
    </Box>
  );
};
