/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable no-extra-boolean-cast */

import { Box, Button, ListItem } from "@mui/material";
import { useMatch, useNavigate, useResolvedPath } from "react-router-dom";

interface INavItemProps {
  to: string;
  icon: JSX.Element;
  label: string;
  onClick: (() => void) | undefined;
}

export const NavItem: React.FC<INavItemProps> = ({
  to,
  icon,
  label,
  onClick,
}) => {
  const navigate = useNavigate();

  const resolvedPath = useResolvedPath(to);
  const match = useMatch({ path: resolvedPath.pathname, end: false });

  const handleClick = () => {
    navigate(to);
    onClick?.();
  };

  return (
    <ListItem
      disableGutters
      sx={{
        display: "flex",
        mb: 0.5,
        py: 0,
        px: 2,
      }}
    >
      <Button
        component="a"
        startIcon={icon}
        disableRipple
        onClick={handleClick}
        sx={
          {
            backgroundColor: !!match && "rgba(255,255,255, 0.08)",
            borderRadius: 1,
            color: !!match ? "secondary.main" : "#D1D5DB",
            fontWeight: !!match && "fontWeightBold",
            justifyContent: "flex-start",
            px: 3,
            textAlign: "left",
            textTransform: "none",
            width: "100%",
            "& .MuiButton-startIcon": {
              color: !!match ? "secondary.main" : "#9CA3AF",
            },
            "&:hover": {
              backgroundColor: "rgba(255,255,255, 0.08)",
            },
          } as any
        }
      >
        <Box sx={{ flexGrow: 1 }}>{label}</Box>
      </Button>
    </ListItem>
  );
};
