import { Box, Button, Card, CardContent, InputAdornment, SvgIcon, TextField, Typography } from "@mui/material";

import { Search as SearchIcon } from "../../icons";

export const ServiceListToolbar: React.FC = () => {
  return (
    <Box>
      <Box
        sx={{
          alignItems: "center",
          display: "flex",
          justifyContent: "space-between",
          flexWrap: "wrap",
          m: -1,
        }}
      >
        <Typography sx={{ m: 1 }} variant="h4">
          Serviços utilizados pela RD
        </Typography>
        <Box sx={{ m: 1 }}>
          <Button color="primary" variant="contained">
            Novo Serviço
          </Button>
        </Box>
      </Box>
      <Box sx={{ mt: 3 }}>
        <Card>
          <CardContent>
            <Box sx={{ maxWidth: 500 }}>
              <TextField
                fullWidth
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">
                      <SvgIcon fontSize="small" color="action">
                        <SearchIcon />
                      </SvgIcon>
                    </InputAdornment>
                  ),
                }}
                placeholder="Procurar Serviço"
                variant="outlined"
              />
            </Box>
          </CardContent>
        </Card>
      </Box>
    </Box>
  );
};
