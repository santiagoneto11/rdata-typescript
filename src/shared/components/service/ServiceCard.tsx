/* eslint-disable @typescript-eslint/no-explicit-any */

import {
  Avatar,
  Box,
  Card,
  CardContent,
  Divider,
  Grid,
  Typography,
} from "@mui/material";

import { Clock as ClockIcon, Download as DownloadIcon } from "../../icons";

interface IServiceCardProps {
  description: string;
  media: string;
  title: string;
  totalClicks: string;
}

export const ServiceCard: React.FC<IServiceCardProps> = ({
  description,
  media,
  title,
  totalClicks,
}) => {
  return (
    <Card
      sx={{
        display: "flex",
        flexDirection: "column",
        height: "100%",
      }}
    >
      <CardContent>
        <Box
          sx={{
            display: "flex",
            justifyContent: "center",
            pb: 3,
          }}
        >
          <Avatar alt="Service" src={media} variant="square" />
        </Box>
        <Typography
          align="center"
          color="textPrimary"
          gutterBottom
          variant="h5"
        >
          {title}
        </Typography>
        <Typography align="center" color="textPrimary" variant="body1">
          {description}
        </Typography>
      </CardContent>
      <Box sx={{ flexGrow: 1 }} />
      <Divider />
      <Box sx={{ p: 2 }}>
        <Grid container spacing={2} sx={{ justifyContent: "space-between" }}>
          <Grid
            item
            sx={{
              alignItems: "center",
              display: "flex",
            }}
          >
            <ClockIcon color="action" />
            <Typography
              color="textSecondary"
              display="inline"
              sx={{ pl: 1 }}
              variant="body2"
            >
              Atualizado 2 horas atrás
            </Typography>
          </Grid>
          <Grid
            item
            sx={{
              alignItems: "center",
              display: "flex",
            }}
          >
            <DownloadIcon color="action" />
            <Typography
              color="textSecondary"
              display="inline"
              sx={{ pl: 1 }}
              variant="body2"
            >
              {totalClicks} Vezes Usado
            </Typography>
          </Grid>
        </Grid>
      </Box>
    </Card>
  );
};
