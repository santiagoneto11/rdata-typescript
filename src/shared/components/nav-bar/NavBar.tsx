import {
  AppBar,
  Avatar,
  Badge,
  Box,
  Icon,
  IconButton,
  Toolbar,
  Tooltip,
  useMediaQuery,
  useTheme,
} from "@mui/material";

import { useDrawerContext } from "../../contexts";

export const NavBar: React.FC = () => {
  const theme = useTheme();
  const lgDown = useMediaQuery(theme.breakpoints.down("lg"));

  const { toggleDrawerOpen } = useDrawerContext();

  return (
    <AppBar position="static" color="inherit">
      <Toolbar>
        {lgDown && (
          <IconButton onClick={toggleDrawerOpen}>
            <Icon>menu</Icon>
          </IconButton>
        )}
        <IconButton>
          <Icon fontSize="small">search</Icon>
        </IconButton>
        <Box sx={{ flexGrow: 1 }} />
        <Tooltip title="Contatos">
          <IconButton sx={{ ml: 1 }}>
            <Icon>people</Icon>
          </IconButton>
        </Tooltip>
        <Tooltip title="Notificações">
          <IconButton sx={{ ml: 1 }}>
            <Badge badgeContent={4} color="primary" variant="dot">
              <Icon>notifications</Icon>
            </Badge>
          </IconButton>
        </Tooltip>
        <Avatar
          sx={{
            height: 40,
            width: 40,
            ml: 1,
          }}
          src="/static/images/avatars/santiago.png"
        />
      </Toolbar>
    </AppBar>
  );
};
