import {
  Box,
  Button,
  Card,
  CardHeader,
  Divider,
  IconButton,
  List,
  ListItem,
  ListItemAvatar,
  ListItemText,
} from "@mui/material";

import ArrowRightIcon from "@mui/icons-material/ArrowRight";
import MoreVertIcon from "@mui/icons-material/MoreVert";
import { formatDistanceToNow, subHours } from "date-fns";
import { v4 as uuid } from "uuid";

const services = [
  {
    id: uuid(),
    name: "Vini.io",
    imageUrl: "/static/images/services/config.png",
    updatedAt: subHours(Date.now(), 2),
  },
  {
    id: uuid(),
    name: "Manta",
    imageUrl: "/static/images/services/config.png",
    updatedAt: subHours(Date.now(), 2),
  },
  {
    id: uuid(),
    name: "Gitlab",
    imageUrl: "/static/images/services/gitlab.png",
    updatedAt: subHours(Date.now(), 3),
  },
  {
    id: uuid(),
    name: "AWS",
    imageUrl: "/static/images/services/aws.png",
    updatedAt: subHours(Date.now(), 5),
  },
  {
    id: uuid(),
    name: "Teams",
    imageUrl: "/static/images/services/teams.png",
    updatedAt: subHours(Date.now(), 9),
  },
];

export const LatestServices: React.FC = () => (
  <Card sx={{ height: "100%" }}>
    <CardHeader
      subtitle={`${services.length} in total`}
      title="Últimos Serviços"
    />
    <Divider />
    <List>
      {services.map((service, i) => (
        <ListItem divider={i < services.length - 1} key={service.id}>
          <ListItemAvatar>
            <img
              alt={service.name}
              src={service.imageUrl}
              style={{
                height: 48,
                width: 48,
              }}
            />
          </ListItemAvatar>
          <ListItemText
            primary={service.name}
            secondary={`Updated ${formatDistanceToNow(service.updatedAt)}`}
          />
          <IconButton edge="end" size="small">
            <MoreVertIcon />
          </IconButton>
        </ListItem>
      ))}
    </List>
    <Divider />
    <Box
      sx={{
        display: "flex",
        justifyContent: "flex-end",
        p: 2,
      }}
    >
      <Button
        color="primary"
        endIcon={<ArrowRightIcon />}
        size="small"
        variant="text"
      >
        Ver todos
      </Button>
    </Box>
  </Card>
);
