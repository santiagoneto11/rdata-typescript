/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable no-extra-boolean-cast */

import {
  Divider,
  Drawer,
  List,
  Typography,
  useMediaQuery,
  useTheme,
} from "@mui/material";

import { Box } from "@mui/system";
import { useDrawerContext } from "../../contexts";
import { Logo } from "../../icons/logo";
import { Selector as SelectorIcon } from "../../icons/selector";
import { NavItem } from "../nav-item/NavItem";

export const SideBar: React.FC = ({ children }) => {
  const theme = useTheme();
  const lgDown = useMediaQuery(theme.breakpoints.down("lg"));

  const { isDrawerOpen, drawerOptions, toggleDrawerOpen } = useDrawerContext();

  return (
    <>
      <Drawer
        open={isDrawerOpen}
        variant={lgDown ? "temporary" : "permanent"}
        onClose={toggleDrawerOpen}
        PaperProps={{
          sx: {
            backgroundColor: "#111827",
            color: "#FFFFFF",
            width: theme.spacing(35),
          },
        }}
      >
        <Box
          width={theme.spacing(35)}
          height="100%"
          display="flex"
          flexDirection="column"
        >
          <Box sx={{ p: 3 }}>
            <Logo
              sx={{
                height: 42,
                width: 42,
              }}
            />
          </Box>
          <Box sx={{ px: 2 }}>
            <Box
              sx={{
                alignItems: "center",
                backgroundColor: "rgba(255, 255, 255, 0.04)",
                cursor: "pointer",
                display: "flex",
                justifyContent: "space-between",
                px: 3,
                py: "11px",
                borderRadius: 1,
              }}
            >
              <div>
                <Typography color="inherit" variant="subtitle1">
                  RData
                </Typography>
                <Typography color="#9CA3AF" variant="body2">
                  Nível de Usuário : Admin
                </Typography>
              </div>
              <SelectorIcon
                sx={{
                  color: "#6B7280",
                  width: 14,
                  height: 14,
                }}
              />
            </Box>
          </Box>

          <Divider
            sx={{
              borderColor: "#2D3748",
              my: 3,
            }}
          />

          <Box flex={1}>
            <List component="nav">
              {drawerOptions.map((drawerOption) => (
                <NavItem
                  key={drawerOption.path}
                  to={drawerOption.path}
                  icon={drawerOption.icon}
                  label={drawerOption.label}
                  onClick={lgDown ? toggleDrawerOpen : undefined}
                />
              ))}
            </List>
          </Box>
          <Divider sx={{ borderColor: "#2D3748" }} />
          <Box
            sx={{
              px: 2,
              py: 3,
            }}
          ></Box>
        </Box>
      </Drawer>

      <Box
        minHeight="100vh"
        height="100%"
        marginLeft={lgDown ? theme.spacing(0) : theme.spacing(35)}
      >
        {children}
      </Box>
    </>
  );
};
