import {
  Avatar,
  Box,
  Button,
  Card,
  CardActions,
  CardContent,
  Divider,
  Typography,
} from "@mui/material";
import React from "react";

interface IUser {
  avatar: string;
  city: string;
  country: string;
  jobTitle: string;
  name: string;
  timezone: string;
}

const user: IUser = {
  avatar: "/static/images/avatars/santiago.png",
  city: "Fortaleza",
  country: "Brasil",
  jobTitle: "Engenheiro de Dados",
  name: "Santiago Neto",
  timezone: "GMT -3",
};

export const AccountProfile: React.FC = () => {
  return (
    <Card>
      <CardContent>
        <Box
          sx={{
            alignItems: "center",
            display: "flex",
            flexDirection: "column",
          }}
        >
          <Avatar
            src={user.avatar}
            sx={{
              height: 64,
              mb: 2,
              width: 64,
            }}
          />
          <Typography color="textPrimary" gutterBottom variant="h5">
            {user.name}
          </Typography>
          <Typography color="textSecondary" variant="body2">
            {`${user.city} ${user.country}`}
          </Typography>
          <Typography color="textSecondary" variant="body2">
            {user.timezone}
          </Typography>
        </Box>
      </CardContent>
      <Divider />
      <CardActions>
        <Button color="primary" fullWidth variant="text">
          Alterar Foto
        </Button>
      </CardActions>
    </Card>
  );
};
